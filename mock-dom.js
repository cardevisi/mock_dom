(function(){

	window.doc = document.implementation.createHTMLDocument('MyMockPage');

	function Utils (scope) {
		
		var doc = (scope) ? scope : document;
		
		this.createScriptTags = function (obj) {
			if (!obj.src || typeof obj.src !== 'string') return false;
			
			var script, type = (obj.insertType) ? obj.insertType : 'appendChild';
			
			if (type !== "write") {
				var node = (obj.node) ? obj.node : 'head';
				script = doc.createElement('script');
				script.src = obj.src;
			} else {
				script = "<sc" + "ript type='text/javascript' language='JavaScript' src='"+obj.src+"'></sc" + "ript>";
			}

			try {
				if(type === "write") {
					doc[type](script);
				} else {
					doc[node][type](script);
				}
			} catch(err) {
				console.log('não foi possível executar o método createScriptTags');
			}
		};
		
		this.scriptInPage = function(domain, callback) {
			var scripts = doc.getElementsByTagName('script');
	    	var total = scripts.length;
	        while (total--) {
	            if (scripts[total].hasAttribute("src") && (scripts[total]["src"].indexOf(domain) > -1)) {
	                if (callback) {
	                    callback(scripts[total])
	                }
	            }
	        }
	    };

	    this.removeAllScripts = function () {
	    	var scripts = doc.getElementsByTagName('script');
	    	var total = scripts.length;
			while (total--) {
	       		var node = scripts[total].parentNode;
	       		node.removeChild(scripts[total]);
	       	};
	    };

	    this.findValuesInArray = function(arr, value) {
	    	return arr.indexOf(value) !== -1;
	    };

	    this.searchValuesInArray = function(arr, value) {
	    	var pos = (arr.indexOf(value) !== -1) ? arr.indexOf(value) : undefined;
	    	if (pos === undefined) return;
	    	return arr[pos] ? arr[pos] : undefined;
	    };
	}

	var utils = new Utils();
	console.log('utils', utils);

	var domain = "http://teste1.com.br";
	for (var i = 0; i < 10; i++) {
		var url = "http://estudo.intranet.uol.com.br/mock_dom/mock"+i+".js";

		console.log('url', url);
	
		utils.createScriptTags({
			'src': url,
			'node': 'head',
			'insertType': 'appendChild'
		});
	};

	//searchByRegularExpression
	
	console.log("findValuesInArray=>", utils.findValuesInArray([4, 5, 10], 11));
    console.log("searchValuesInArray =>", utils.searchValuesInArray([4, 5, 10, 1000, 2000, 500, 90, "/http:\/\/www\.teste\.com\.br/"], 2000));
	//console.log("removeAllScripts =>", utils.removeAllScripts());
	doc.write("<sc" + "ript type='text/javascript' language='JavaScript' src='http://www.uol.com.br'></sc" + "ript>");
	console.log(utils.scriptInPage(), doc);


	function onReady() {
		if(document.addEventListener) {
		    document.addEventListener('load', function () {
		        ready('chamada 1');
		    }, false);
		} else if (document.attachEvent) {
		    document.attachEvent("onreadystatechange", function() {
		        if ( document.readyState === "complete" ) {
		            ready('chamada 2');
		        }
		    });
		} else if (window.addEventListener) {
		     window.addEventListener('load', function () {
		        ready('chamada 3');
		     }, false);
		} else if (window.attachEvent) {
		    window.attachEvent('onload', function () {
		        ready('chamada 4');
		    });
		} else {
		    var fn = window.onload; // very old browser, copy old onload
		    window.onload = function() { // replace by new onload and call the old one
		        ready('chamada 5');
		    };
		}
	};

	TM.documentReady = function(callback) {
    if(document.addEventListener) {
        document.addEventListener( "DOMContentLoaded", function(){
            callback('chamada 1');
        }, false );
    } else if (document.attachEvent) {
        document.attachEvent("onreadystatechange", function() {
            if (document.readyState === "complete" ) {
                callback('chamada 2');
            }
        });
    }

    //***************************************
    //Removed Event in anonymous function
    //***************************************
	button.addEventListener('click', function(e){
    	this.removeEventListener(e.type, arguments.callee);
    });


};
	
})();