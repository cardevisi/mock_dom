function documentReady(callback, timeout) {
    if(!timeout && document.addEventListener) {
        document.addEventListener( "DOMContentLoaded", function(){
            callback('chamada 1');
        }, false );
    } else if (!timeout && document.attachEvent) {
        document.attachEvent("onreadystatechange", function() {
            if (document.readyState === "complete" ) {
                callback('chamada 2');
            }
        });
    } else if(timeout) {
        setTimeout(function(){
            callback('chamada 3');
        }, timeout);
    }
};

function ready (fn) {
    console.log('CHAMANDO documentReady', fn);
};

documentReady(ready);

function Tag () {
    this.setup = 1;
    this.init = function (value) {
        console.log('init Tag', value);
    };
}

function TagExtend () {
    Tag.call(this);
    this.init("Fala");
}

var tag = new TagExtend();
console.log(tag);
    
